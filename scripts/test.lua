print('name: ' .. os.name())

env.print('pwd: ' .. tostring(env.pwd()))

fs.mkdir('/tmp/lush-ut')

env.print('Zipping files')
files.zip("/tmp/lush-ut/new_post.zip", "src")

env.print('pushd to /tmp/lush-ut')
env.pushd('/tmp/lush-ut')
env.print('Unzipping files')
files.unzip("new_post.zip")
env.print('popd from /tmp/lush-ut')
env.popd()

env.print('Removing /tmp/lush-ut')
fs.rmdir('/tmp/lush-ut', { recursive = true })

env.print('Entering /tmp')
env.cd('/tmp')
env.print('Listing files')
local files = fs.ls() -- '*.md'
for _, file in ipairs(files) do
    env.print("- ", file)
end

env.set('NAME', 'Thiago')
env.print('ENV: ' .. env.get('NAME'))
env.del('NAME')
env.print('ENV: ' .. tostring(env.get('NAME')))

env.print('os name: ' .. os.name())
